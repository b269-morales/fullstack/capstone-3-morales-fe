import {useState, useEffect, useContext} from 'react';
import {Form, Button, Spinner} from 'react-bootstrap';
import Swal from 'sweetalert2';

// import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

export default function Login(){

	// "useContext" hook that allows you to access and update the shared state from any component in your application.
	// "UserContext" - global state that can be shared and updated throughout the application
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [cart, setCart] = useState('')
	const [isActive, setIsActive] = useState(false)
	const [isLoading, setIsLoading] = useState(false);

	// hook returns a function that lets you navigate to components
	// const navigate = useNavigate()

	function authenticate(e){
		e.preventDefault()
		setIsLoading(true);
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// We will receive either a token or an error response.
			console.log(data);

			// If no user information is found, the "access" property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
			if(typeof data.access !== "undefined") {
				// The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
				console.log(data)
			} else {
				setIsLoading(false);
				Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Please check your login details and try again!"
				})
			}
	});
		
};
	const retrieveUserDetails = (token) => {
			// The token will be sent as part of the request's header information
			// We put "Bearer" in front of the token to follow implementation standards for JWTs
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				// Global user state for validation accross the whole app
				// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
					isSeller: data.isSeller,
					cart: data.cart,
					name: data.firstName
				})
				setIsLoading(false);
				Swal.fire({
					title: "Login Successful!",
					icon: "success",
					text: `Welcome to Elevate, ${data.firstName}`
				})
			})
		};

	useEffect(() => {
		if((email !== '' && password !== '')){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id !== null) ?
		<Navigate to="/" />
		:
		<Form onSubmit={e => authenticate(e)} className="mt-5 mx-auto half-width-form" >
			<Form.Group controlId="userEmail" className="turn-white">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password" className="turn-white">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			{isLoading ? (
			  <Button variant="success" type="submit" id="submitBtn" className="mt-3" disabled>
				<Spinner animation="border" size="sm" /> Loading...
			  </Button>
			) : isActive ? (
			  <Button variant="success" type="submit" id="submitBtn" className="mt-3">
				Submit
			  </Button>
			) : (
			  <Button variant="danger" type="submit" id="submitBtn" disabled className="mt-3">
				Submit
			  </Button>
			)}
		</Form>
	)
};
