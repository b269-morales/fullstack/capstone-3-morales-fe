import Banner from '../components/Banner'
import UserContext from '../UserContext'
import {useContext} from 'react';

export default function Home() {

	const {user} = useContext(UserContext);

	console.log(user)
	return(
		<>
		{user ? (
						<Banner id="bannerHomeReturn" />
					) : (
						<Banner id="bannerHomeNew" />
					)}
		</>
		)
}
