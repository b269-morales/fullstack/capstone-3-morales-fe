import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';

import Swal from 'sweetalert2';

import {useParams, useNavigate, Link} from 'react-router-dom';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const Navigate = useNavigate();
	// "useParams" hook that will allow us to retrieve the courseId passed via URL params
	// http://localhost:3000/courses/642b9ee764ba1150b3a1a4e1
	const {productId} = useParams();
	
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stock, setStock] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);


	function editProduct(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: description,
				price: price,
				stock: stock,
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Successfully updated product!",
					icon: "success",
					text: `You have successfully updated ${productName}.`
				})

				Navigate(`/products/${productId}`)

			} else if (data === false) {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			} 
		});
	}



useEffect(() => {
	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.productName);
			setDescription(data.productDescription);
			setPrice(data.productPrice);
			setStock(data.stock);
			setIsActive(data.isActive)
		})
}, [productId])

	useEffect(() => {
		if (
			productName !== "" &&
			description !==""&&
			price !==""&&
			stock !== "" && 
			isActive !== "" 
			)
			{setIsValid(true);
		} else {
			setIsValid(false)
		}
	}, [productName, description, stock]);



	return (
		(user.isAdmin === true || user.isSeller === true) ?
		<Form onSubmit={e => editProduct(e)} className="mt-5 mx-auto half-width-form">

			<Form.Group controlId="productName" className="turn-white mt-4">
				<Form.Label>Product Name</Form.Label>
				<Form.Control 
					type="productName" 
					placeholder="Enter Product Name" 
					value={productName}
					onChange={(e) => setProductName(e.target.value)}
					required

				/>
			</Form.Group>

			 <Form.Group controlId="description" className="turn-white mt-4">
				<Form.Label>Product Description</Form.Label>
				<Form.Control 
					type="description" 
					placeholder="Enter product description" 
					value={description}
					onChange={(e) => setDescription(e.target.value)}
					required

				/>
			</Form.Group>

			<Form.Group controlId="price" className="turn-white mt-4">
				<Form.Label>Product Price</Form.Label>
				<Form.Control 
					type="price" 
					placeholder="Enter product price (in Pesos)" 
					value={price}
					onChange={(e) => setPrice(e.target.value)}
					required

				/>
			</Form.Group>


			<Form.Group controlId="stock" className="turn-white mt-4">
				<Form.Label>Stock</Form.Label>
				<Form.Control 
					type="stock" 
					placeholder="Enter product stocks" 
					value={stock}
					onChange={(e) => setStock(e.target.value)}
					required

				/>
			</Form.Group>

			<Form.Group controlId="isActive" className="turn-white mt-4">
			  <Form.Label>Product Visibility</Form.Label>
			  <div style={{ display: "flex", alignItems: "center" }}>
				<Button
				  variant={isActive ? "success" : "danger"}
				  onClick={() => setIsActive(!isActive)}
				>
				  {isActive ? "Visible" : "Hidden"}
				</Button>
				<Form.Text className="text-muted" style={{ paddingLeft: '10px' }}>
				  Will the product be visible right after updating?
				</Form.Text>
			  </div>
			</Form.Group>



			{isValid ?
			<div className="d-flex justify-content-center">
				<Button className="my-4" variant="danger" type="submit" id="submitBtn" size="lg">
					Update
				</Button>
			</div>

			:
			<div className="d-flex justify-content-center">
				<Button className="my-4" variant="danger" type="submit" id="submitBtn" size="lg" disabled>
					Update
				</Button>
			</div>
			}

			
		</Form>
		:
		<Navigate to="/noaccess"/>
	
	)


}