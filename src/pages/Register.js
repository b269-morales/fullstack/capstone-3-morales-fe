import {useState, useEffect, useContext} from 'react';

import { Form, Button } from 'react-bootstrap';

import UserContext from '../UserContext';

import {useNavigate, Navigate} from 'react-router-dom'

import Swal from 'sweetalert2';


export default function Register() {

	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [userName, setUserName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);
	const {user} = useContext(UserContext);



	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userName: userName,
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1

			})
		})
		.then(res => res.json())
		.then(data => {
		console.log(data)

		if(data === true) {
			Swal.fire({
				title: "Successfully registered!",
				icon: "success",
				text: "You have successfully registered."
			})

			navigate("/login")

		} else if (data === false) {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		} else {
			Swal.fire({
				title: "Email or mobile number is already registered.",
				icon: "error",
				text: "Please try using a different email or number."
			})
		}

	});

};



	useEffect(() => {
		if (
			password1 === password2 &&
			userName !== "" &&
			firstName !==""&&
			lastName !==""&&
			email !== "" && 
			password1 !== "" && 
			password2 !== "" &&
			mobileNo.length == 11
	
			)
			{setIsActive(true);
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2, firstName, lastName, mobileNo, userName]);



	return (
		(user.id !== null) ?
		<Navigate to="/course"/>
				:
		<Form onSubmit={e => registerUser(e)} className="mt-5 mx-auto half-width-form">

			<Form.Group controlId="firstName" className="turn-white mt-2">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="firstName" 
					placeholder="Enter First Name" 
					value={firstName}
					onChange={(e) => setFirstName(e.target.value)}
					required

				/>
			</Form.Group>

			 <Form.Group controlId="lastName" className="turn-white mt-2">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="lastName" 
					placeholder="Enter Last Name" 
					value={lastName}
					onChange={(e) => setLastName(e.target.value)}
					required

				/>
			</Form.Group>

			<Form.Group controlId="userName" className="turn-white mt-2">
				<Form.Label>Username</Form.Label>
				<Form.Control 
					type="userName" 
					placeholder="Enter Username" 
					value={userName}
					onChange={(e) => setUserName(e.target.value)}
					required

				/>
			</Form.Group>


			<Form.Group controlId="userEmail" className="turn-white mt-2">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter email" 
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required

				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			 <Form.Group controlId="mobileNumber" className="turn-white mt-2">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="mobileNumber" 
					placeholder="Enter Mobile Name" 
					value={mobileNo}
					onChange={(e) => setMobileNo(e.target.value)}
					required

				/>
			</Form.Group>

			<Form.Group controlId="password1" className="turn-white mt-2">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Password" 
					value={password1}
					onChange={(e) => setPassword1(e.target.value)}
					required

				/>
			</Form.Group>

			<Form.Group controlId="password2" className="turn-white mt-2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Verify Password" 
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
					required

				/>
			</Form.Group>   

			{isActive ?
				<Button className="my-3" variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>

			:
				<Button className="my-3" variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			}

			
		</Form>
	
	)

}


