import { Row, Col, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import bannerData from '../data/bannerData';

export default function Banner({ id }) {
	const banner = bannerData.find((banner) => banner.id === id);
	const { name, description, button, dest } = banner;

	const navigate = useNavigate();

	function destination(e) {
		e.preventDefault();
		navigate(`/${dest}`);
	}



	return (
		<Row>
			<Col className="p-5 turn-white">
				<h1>{name}</h1>
				<p>{description}</p>
				<Button variant="danger" onClick={destination}>{button}</Button>
			</Col>
		</Row>
	);
}


