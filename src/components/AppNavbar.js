import { Link, NavLink } from 'react-router-dom';
import { useState, useContext } from 'react';
import { Navbar, Nav, NavDropdown, Button, Offcanvas, Form, FormControl, Container } from 'react-bootstrap';
import { FaRegUserCircle, FaSearch } from 'react-icons/fa';

import UserContext from '../UserContext'
import UserCart from './UserCart';
import UserOrders from './UserOrders';


export default function AppNavbar() {

	// to store user information stored in the login page
	// const [user, setUser]= useState(localStorage.getItem('email'));

	const { user } = useContext(UserContext);

	const [show, setShow] = useState(false);
	const [searchTerm, setSearchTerm] = useState('');

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const handleSearch = (e) => {
		e.preventDefault();
		// Do something with the search term
		console.log(searchTerm);
	};

	return (
		<>
			<div>
				<Navbar bg="dark" variant="dark" expand="lg" className="px-4">
					<Navbar.Brand as={Link} to="/">
						Elevate
					</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
						<Nav className="ml-auto">
							{user.id !== null ? (
								user.isSeller === true ? (
									<>
										<Nav.Link as={NavLink} to="/products">
											Products
										</Nav.Link>
										<Nav.Link as={NavLink} to="/addproduct">
											Add Product
										</Nav.Link>
										<Nav.Link as={NavLink} to="/logout">
											Logout
										</Nav.Link>
									</>
								) : user.isAdmin === true ? (
									<>
										<Nav.Link as={NavLink} to="/addplayer">
											Add Player
										</Nav.Link>
										<Nav.Link as={NavLink} to="/addproduct">
											Add Product
										</Nav.Link>
										<Nav.Link as={NavLink} to="/allproducts">
											All Products
										</Nav.Link>
										<Nav.Link as={NavLink} to="/products">
											Active Products
										</Nav.Link>
										<Nav.Link as={NavLink} to="/logout">
											Logout
										</Nav.Link>
									</>
								) : (
									<>
										<Nav.Link as={NavLink} to="/products">
											Products
										</Nav.Link>
										<UserCart />
										<UserOrders />
										<Nav.Link as={NavLink} to="/logout">
											Logout
										</Nav.Link>
										<NavDropdown
											id="basic-nav-dropdown"
											align="end"
											title={<FaRegUserCircle style={{ color: 'white' }} />}
										>
											<NavDropdown.Item href="#action/3.1">
												Action
											</NavDropdown.Item>
											<NavDropdown.Item href="#action/3.2">
												Another action
											</NavDropdown.Item>
											<NavDropdown.Item href="#action/3.3">
												Something
											</NavDropdown.Item>
											<NavDropdown.Divider />
											<NavDropdown.Item href="#action/3.4">
												Separated link
											</NavDropdown.Item>
										</NavDropdown>
									</>
								)
							) : (
								<>
									<Nav.Link as={NavLink} to="/register">
										Register
									</Nav.Link>
									<Nav.Link as={NavLink} to="/login">
										Login
									</Nav.Link>
								</>
							)}
						</Nav>
					</Navbar.Collapse>
				</Navbar>
			</div>
			<div className="navbar-container">
				<Container fluid className="p-0">
					<Navbar bg="dark" variant="dark" expand="lg" className="px-3">
						<Navbar.Collapse id="basic-navbar-nav" className='justify-content-center pb-3'>
							<Nav className="ml-auto d-flex align-items-center">
								<Form inline onSubmit={handleSearch}>
									<FormControl
										type="text"
										placeholder="Search"
										className="ml-5 search-bar"
										value={searchTerm}
										onChange={(e) => setSearchTerm(e.target.value)}
									/>
								</Form>
							</Nav>
						</Navbar.Collapse>
					</Navbar>
				</Container>
			</div>
		</>
	);
}

