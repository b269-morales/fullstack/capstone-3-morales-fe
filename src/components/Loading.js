import { useState, useEffect } from 'react';
import { Spinner } from 'react-bootstrap';

function LoadingIndicator({ isLoading }) {
	const [showSpinner, setShowSpinner] = useState(false);

	useEffect(() => {
		let timeoutId;
		if (isLoading) {
			// If isLoading is true, show the spinner after 500ms
			timeoutId = setTimeout(() => setShowSpinner(true), 500);
		} else {
			// If isLoading is false, hide the spinner immediately
			setShowSpinner(false);
		}

		// Cleanup function to clear the timeout when component unmounts
		return () => clearTimeout(timeoutId);
	}, [isLoading]);

	return (
		showSpinner && (
			<div className="d-flex justify-content-center my-3">
				<Spinner animation="border" role="status">
					<span className="sr-only">Loading...</span>
				</Spinner>
			</div>
		)
	);
}

export default LoadingIndicator;
